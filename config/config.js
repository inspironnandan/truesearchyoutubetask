/*
 *******************************************************************************
 *
 *          %name:  config.js %
 *    %derived_by:  Nikhil %
 *
 *       %version:  1 %
 *       %release:  youtubeSync/1.0 %
 * %date_modified:  Mon Jan 13 14:00:00 2020 %
 *
 * Date          By             Description
 * ------------  -----------    -----------
 * Jan 13, 2020  Nikhil           created
 *******************************************************************************
 */
/*jslint node: true */
"use strict";
var winston = require("winston");
var packageJson = require("../package.json");
var moment = require("moment");
var config = {};
/*
/**
 * Basic App Configuration.
 */
config.APP_VERSION = packageJson.version; // app version is read from package.json
config.APP_TITLE = packageJson.description; // app title is read from package.json
config.APP_STARTUP_FILE = "youtubeSync.js"; // start-up file, Don't change this configuration
config.SERVER = {
  PORT: 9009, // Port that application server will run on. If this is changed, the Passenger command's --port parameter has to match this.
  MIN_INSTANCES: 1, // Number of clusters the app will have. This should be (no. of CPU cores - 1), and the Passenger command's --min-instances parameter has to match this.
  MAX_POOL_SIZE: 1, // Maximum number of instances that will be maintained in a static pool, and the Passenger command's --max-pool-size parameter has to match this.
  FRIENDLY_ERROR_PAGES: false, // Set to `true` to show Passenger's friendly error page, which contains a lot of information that can help in debugging.
  ENVIRONMENT: "production",
  TIMEOUT: 0
};
config.MYSQL = {
  host: "dummy",
  user: "dummy",
  password: "dummy",
  database: "dummy",
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
};
config.LOG = {
  FILENAME: "./logs/youtubeSync.%DATE%.log", // Main log file's path and name. `FORMAT` is appended to `FILENAME`.
  FORMAT: "DD-MM-YYYY", // Date pattern in filename. Allowed tokens are yy, yyyy, M, MM, d, dd, H, HH, m, mm.
  PRETTY_PRINT: false, // Set to `true` if util.inspect should be used on metadata. Helps in debugging.
  CONSOLE_PRINT: false, // Set to `true` if output should also additionally be printed on console.
  JSON: false, // Set to `true` if log file should be in JSON format. Helps when log data needs to be consumed by external service like Loggly.
  COLORIZE: false, // Set to `true` if output should be colorized. Use only when `CONSOLE_PRINT` is also set to `true`.
  TIMESTAMP: function(args) {
    return moment().format("DD-MM-YYYY HH:mm:ss.SSS");
  }, // Set to `true` to include timestamp in log.
  LEVEL: "debug", // Logging level; allowed values are 'silly' (lowest), 'verbose', 'debug', 'info', 'warn', 'error' (highest).
  MAXSIZE: 500000000 // 500MB  maxsize option which will rotate the logfile when it exceeds a certain size in bytes.
};
winston.add(require("winston-daily-rotate-file"), {
  filename: config.LOG.FILENAME,
  name: "mainLog", // Internal name for logging instance.
  datePattern: config.LOG.FORMAT,
  handleExceptions: true, // Automatically log all unhandled exceptions.
  exitOnError: false, // Set to `true` to exit the application on any unhandled exception. Not recommended.
  prettyPrint: config.LOG.PRETTY_PRINT,
  silent: config.LOG.CONSOLE_PRINT,
  humanReadableUnhandledException: true, // Set to `true` to print clean stacktrace.
  json: config.LOG.JSON,
  colorize: config.LOG.COLORIZE,
  timestamp: config.LOG.TIMESTAMP,
  level: config.LOG.LEVEL,
  maxsize: config.LOG.MAXSIZE,
  localTime: true
});
config.logger = winston;

module.exports = config;
