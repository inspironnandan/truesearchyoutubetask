export const environment = {
  production: true,
  api_url: 'http://ec2-13-235-9-102.ap-south-1.compute.amazonaws.com:9009/',
  SOCKET_ENDPOINT: 'http://ec2-13-235-9-102.ap-south-1.compute.amazonaws.com:9009'
};
