import { Component, OnInit, OnDestroy, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { Socialusers } from '../../core/models/social-users';
import { UserService } from '../../core/services/user.service';
import { SocketioService } from '../../core/services/socketio.service';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';
@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})

export class CommentsComponent implements OnInit, OnDestroy {
  socialusers = new Socialusers();
  youtubeUrl: '';
  youtubeKeywords: any;
  commentList = [];
  private comments: Subscription;
  currentComment: string;
  chatHistory: any[] = [];
  historyKeyword: any;
  @ViewChild('chatdata', {static: true}) chatdata: ElementRef;
  constructor(
    private router: Router,
    private userService: UserService,
    private oauthService: OAuthService,
    private socketIo: SocketioService,
    private renderer: Renderer2
    ) { }
  ngOnInit() {
    this.socialusers = JSON.parse(localStorage.getItem('socialusers'));
    this.socketIo.data.subscribe(data => {
      if (data) {
      const p: HTMLParagraphElement = this.renderer.createElement('div');
      p.innerHTML = data;
      p.className = 'p-2 chat-msg mb-2';
      this.renderer.appendChild(this.chatdata.nativeElement, p);
      }
    });
  }
  logout() {
    this.userService.logoutUser();
    this.oauthService.logOut();
  }
  getComments() {
    let keys = '';
    if ( this.youtubeKeywords && this.youtubeKeywords.trim() !== '') {
      keys = this.youtubeKeywords ? this.youtubeKeywords.split(',') : '';
    }
    this.userService.getComments(this.youtubeUrl, keys).subscribe( (data) => {
     this.socketIo.setupSocketConnection();
   });
  }
  ngOnDestroy() {
    this.comments.unsubscribe();
  }

  loadDoc(id: string) {
    this.userService.getComments(this.youtubeUrl, this.youtubeKeywords);
  }
  getChatHistory() {
    let keys = '';
    if ( this.historyKeyword && this.historyKeyword.trim() !== '') {
      keys = this.historyKeyword ? this.historyKeyword.split(',') : '';
    }
    this.userService.getChatHistory(keys).subscribe( data => {
      this.chatHistory = data['response'];
    });
  }
}
