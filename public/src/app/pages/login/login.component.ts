import { Component, OnInit } from '@angular/core';
// import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angular-6-social-login';
// import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login';
import { Socialusers } from '../../core/models/social-users';
import { UserService } from '../../core/services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';
import { AuthConfig } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  response;
  socialusers = new Socialusers();
  authConfig: AuthConfig = {
    issuer: 'https://accounts.google.com',
    redirectUri: 'http://ec2-13-235-9-102.ap-south-1.compute.amazonaws.com:9009/callback',
    clientId: '77669081051-9ah2a46d46t5vattnq3771arm5ebthde.apps.googleusercontent.com',
    scope: 'https://www.googleapis.com/auth/youtube.readonly https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtube.force-ssl https://www.googleapis.com/auth/userinfo.email',
    strictDiscoveryDocumentValidation: false,
    customQueryParams: {
      'code': ''
    }
  };
  constructor(
    // public OAuth: AuthService,
    private SocialloginService: UserService,
    private router: Router,
    private oauthService: OAuthService
  ) {
    this.oauthService.configure(this.authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
    this.oauthService.setStorage(sessionStorage);
  }

  ngOnInit() {
    console.log(getParamsObjectFromHash());
    if (getParamsObjectFromHash()) {
      this.Savesresponse(getParamsObjectFromHash());
    }
  }
  public socialSignIn(socialProvider: string) {
    // let socialPlatformProvider;
    // if (socialProvider === 'facebook') {
    //   socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    // } else if (socialProvider === 'google') {
    //   socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    // }
    // this.OAuth.signIn(socialPlatformProvider).then(socialusers => {
    //   console.log(socialProvider, socialusers);
    //   console.log(socialusers);
    //   this.Savesresponse(socialusers);
    // });

     this.oauthService.initImplicitFlow();
    // this.oauthService.initImplicitFlowInPopup();
  }
  Savesresponse(socialusers: any) {
    this.SocialloginService.Savesresponse(socialusers).subscribe((res: any) => {
      // debugger;
      // console.log(res['response']['user']);
     // this.socialusers = socialusers;
      // this.response = res.userDetail;
      window.sessionStorage.setItem('socialusers', JSON.stringify( res['response']['user']));
      // console.log(res['response']['user']);
      // console.log(localStorage.setItem('socialusers', JSON.stringify(this.socialusers)));
      this.router.navigate(['/comments']);
    });
  }
}

export function getParamsObjectFromHash() {
  const hash = window.location.hash ? window.location.hash.split('#') : [];
  let toBeReturned = {};
  if (hash.length && hash[1].split('&').length) {
    toBeReturned = hash[1].split('&').reduce((acc, x) => {
      const hello = x.split('=');
      if (hello.length === 2) acc[hello[0]] = hello[1];
        return acc;
    }, {});
  }
  return Object.keys(toBeReturned).length ? toBeReturned : null;
}
