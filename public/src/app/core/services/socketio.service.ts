import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment.prod';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketioService {
  socket;
  private dataSource = new BehaviorSubject<any>('');
  data = this.dataSource.asObservable();
  constructor() {   }

  setupSocketConnection() {
    this.socket =  io(environment.SOCKET_ENDPOINT);
    this.socket.on('chat', (data: string) => {
      console.log(data);
      this.dataSource.next(data);
    });
  }
}
