import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { ApiService } from './api.service';
import { JwtService } from './jwt.service';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  url: any;

  constructor(
    private apiService: ApiService,
    private jwtService: JwtService,
    private http: HttpClient
  ) {}

  get isLoggedIn() {
    if (window.sessionStorage.getItem('socialusers')) {
      this.loggedIn.next(true);
    }
    return this.loggedIn.asObservable();
  }
  setAuth(user: any) {
    this.jwtService.saveToken(user.token);
    this.loggedIn.next(true);
  }
  loggedInUser(user: any) {
    window.sessionStorage.setItem('socialusers', JSON.stringify(user));
  }
  getLoggedInUser(user: any): any {
    return JSON.parse(window.sessionStorage.getItem('socialusers'));
  }
  removeAuth() {
    this.jwtService.destroyToken();
    this.loggedIn.next(false);
    window.sessionStorage.removeItem('socialusers');
    sessionStorage.clear();
  }
  logoutUser() {
    window.sessionStorage.removeItem('socialusers');
    sessionStorage.clear();
  }
  Savesresponse(responce): Observable<any> {
    return this.apiService.post('loginDetails', responce).pipe( map(data => {
      return data;
    })
    );
  }
  get checkLogin(): boolean {
    if (window.sessionStorage.getItem('socialusers')) {
      return true;
    }
  }

  getComments(url, keywords): Observable<any> {
    const user = window.sessionStorage.getItem('socialusers') !== null ?
    JSON.parse(window.sessionStorage.getItem('socialusers')) : '';
    const postData: object = {
      link: url,
      keywords,
      user : window.sessionStorage.getItem('socialusers') !== null ?
    JSON.parse(window.sessionStorage.getItem('socialusers')) : ''
    };
    return this.apiService.post('trackingChat', postData).pipe( map(data => {
      return data;
    })
    );
  }

  getChatHistory(keywords): Observable<any> {
    const postData: object = {
      keywords,
      user : window.sessionStorage.getItem('socialusers') !== null ?
    JSON.parse(window.sessionStorage.getItem('socialusers')) : ''
    };
    return this.apiService.post('chatHistory', postData).pipe( map(data => {
      return data;
    })
    );
  }
}
