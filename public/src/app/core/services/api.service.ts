import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment.prod';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { JwtService } from './jwt.service';

import { catchError, map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
   head = new HttpHeaders({
    'Content-Type': 'application/json',
    Accept: 'application/json'
  });
  constructor(
    private http: HttpClient,
    private jwtService: JwtService
  ) {}

  private formatErrors(error: any) {
    return  throwError(error.error);
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(`${environment.api_url}${path}`, { params })
      .pipe(catchError(this.formatErrors));
  }

  put(path: string, body: object = {}): Observable<any> {
    return this.http.put(
      `${environment.api_url}${path}`,
      JSON.stringify(body)
    ).pipe(catchError(this.formatErrors));
  }

  post(path: string, body: object = {}): Observable<any> {
    const url = `${environment.api_url}${path}`;
    return this.http.post(
        url
      ,
      JSON.stringify(body), {headers: this.head}
    ).pipe(catchError(this.formatErrors));
  }

  getFilePost(path: string, body: object = {}): Observable<any> {
    const url = `${environment.api_url}${path}`;
    // return this.http.post(url, JSON.stringify(body), {responseType: 'blob', observe: 'response'}
    // ).pipe(catchError(this.formatErrors));
    return this.http.post(url, JSON.stringify(body), {responseType: 'text', observe: 'response'}
    ).pipe(catchError(this.formatErrors));
  }
  postfile(path: string, body): Observable<any> {
    const url = `${environment.api_url}${path}`;
    return this.http.post(url, body).pipe(catchError(this.formatErrors));
  }
  delete(path): Observable<any> {
    return this.http.delete(
      `${environment.api_url}${path}`
    ).pipe(catchError(this.formatErrors));
  }
}
