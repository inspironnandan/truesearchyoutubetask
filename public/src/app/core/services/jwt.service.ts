import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor() { }
  getToken(): string {
    return window.sessionStorage.dealerToken;
  }

  saveToken(token: string) {
    window.sessionStorage.dealerToken = token;
  }

  destroyToken() {
    window.sessionStorage.removeItem('dealerToken');
  }

}
