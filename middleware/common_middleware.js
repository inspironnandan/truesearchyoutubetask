/*
 *******************************************************************************
 *
 *          %name:  common_middleware.js %
 *    %derived_by:  Nikhil %
 *
 *       %version:  1 %
 *       %release:  youtubeSync/1.0 %
 * %date_modified:  Mon Jan 13 14:00:00 2020 %
 *
 * Date          By             Description
 * ------------  -----------    -----------
 * Jan 13, 2020  Nikhil 	    created
 *******************************************************************************
 */
/*jslint node: true */
var express = require("express");
var path = require("path");
var reqFilter = express.Router();

/**
 * Common Middleware.
 */
reqFilter.use(function(req, res, next) {
  var url = req.url.split("?")[0];
  if (url === "/" || url === "/login" || url === "/comments") {
    res.sendFile(path.join(global.appRoot, "public/index.html"));
  } else {
    next();
  }
});

module.exports = reqFilter;
