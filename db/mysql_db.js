/*
 *******************************************************************************
 *          %name:  mysql_db.js %
 *    %derived_by:  Nikhil %
 *
 *       %version:  1 %
 *       %release:  youtubeSync/1.0 %
 * %date_modified:  Mon Jan 13 14:00:00 2020 %
 *
 * Date          By             Description
 * ------------  -----------    -----------
 * Jan 13, 2020  Nikhil           created
 *******************************************************************************
 */
/*jslint node: true */
"use strict";
var config = require("../config/config");
var mysqldb = require("mysql"); // orawrap for oracle
var mysql_db = {};
var pool;
var logger = config.logger;

/* Initialising MySQL DB*/
mysql_db.init = function(callback) {
  logger.info("initializing mysql db pool...");
  pool = mysqldb.createPool(config.MYSQL);
  callback();
};

//////////////////////
// GET A CONNECTION //
//////////////////////
mysql_db.doConnect = function(callback) {
  pool.getConnection(function(err, connection) {
    if (err) {
      logger.error("Failed to create connection...");
    } else {
      connection.query("select 1 from dual", function(error, results, fields) {
        if (error) {
          logger.error("Invalid Connection...");
          connection.release(function() {
            callback(err);
          });
        } else {
          logger.info("Successful Connection");
          callback(null, connection);
        }
      });
    }
  });
};

mysql_db.executeSql = function(connection, options, callback) {
  if (!options.bindParams) {
    options.bindParams = {};
  }
  if (!options.opts) {
    options.opts = {};
  }
  connection.query(options.sql, options.bindParams, function(err, result) {
    logger.info("Executing sql: " + options.sql + " , bind params: " + JSON.stringify(options.bindParams) + " , options: " + JSON.stringify(options.opts));
    // Something went wrong - handle the data and release the connection
    if (err) {
      logger.info("ERROR: Unable to execute the SQL: ", err);
      return callback(err);
    }
    // Return the result to the request initiator
    // logger.info("INFO: Result from Database: ", result)
    return callback(err, result);
  });
};

mysql_db.doCommit = function(connection, callback) {
  connection.commit(function(err) {
    if (err) {
      logger.info("ERROR: Unable to COMMIT transaction: ", err);
    }
    return callback(err, connection);
  });
};

mysql_db.doRelease = function(connection, callback) {
  if (connection) {
    connection.release();
    return callback();
  } else {
    return callback();
  }
};
module.exports = mysql_db;
