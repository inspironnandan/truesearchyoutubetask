/**
 * Youtube Live Comment Sync
 * Express API Helper.
 * Author: Nikhil
 * Version: 1.0.0
 * Last Updated: 13-Jan-2019
 */
/*jslint node: true */
"use strict";
/**
 * Module dependencies.
 */

var api = {};

/**
 * Global variables.
 */

var Status = {
  OK: 200,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  UNSUPPORTED_ACTION: 405,
  VALIDATION_FAILED: 422,
  SERVER_ERROR: 500
};

/**
 * Helper functions.
 */
function jsonResponse(res, body, options) {
  options = options || {};
  options.status = options.status || Status.OK;
  res.status(options.status).json(body || null);
}

api.ok = function(req, res, data) {
  jsonResponse(res, data, {
    status: Status.OK
  });
};
api.badRequest = function(req, res, data) {
  jsonResponse(res, data, {
    status: Status.BAD_REQUEST
  });
};

/**
 * Module exports.
 */
module.exports = api;
