/*
 *******************************************************************************
 *
 *          %name:  common_request.js %
 *    %derived_by:  Rakesh %
 *
 *       %version:  1 %
 *       %release:  youtubeSync/1.0 %
 * %date_modified:  Mon Jan 13 14:00:00 2020 %
 *
 * Date          By             Description
 * ------------  -----------    -----------
 * Jan 13, 2020  Rakesh           created
 *******************************************************************************
*/
/*jslint node: true */
'use strict';
var commonRequest = {};
var async = require('async');
var config = require('../../config/config');
var logger = config.logger;

commonRequest.createPaymentCsvFile = function(userName, request, filename, responseCallback) {
    var failureResponse = {
        "responseCode": -1,
        "response": {},
        "errorMsg": "Server Error"
    };
    var sucessResponse = {
        "responseCode": 0,
        "response": {},
        "successMsg": "SUCCESS"
    };
    async.waterfall(
    [
        function(callback) {
            var fileCompletePath = "./public/reports/"+filename;
            fs.appendFile(fileCompletePath,'Trade Name,Firm Name,Invoice Number,Payment Mode,Amount,Payment Date,Payment Type,Cheque Number,NEFT Number,Comments\n',function(err) {
                if (err) {
                    logger.info("err :",err);
                    callback(failureResponse,null);
                } else {
                    callback(null,fileCompletePath);
                }
            });
        }, function(fileCompletePath,callback) {
            for(var i=0;i<request.response.paymentDetails.paymentList.length;i++) {
                var data = request.response.paymentDetails.paymentList[i];
                var text = data.tradeName+','+data.firmName+','+data.invoiceNumber+','+data.paymentMode+','+data.amount+','+data.paymentDate+','+data.paymentType+','+data.chequeNumber+','+data.neftNumber+','+data.comments+'\n';
                fs.appendFile(fileCompletePath,text,function(err) {});
            }
            callback(null,sucessResponse);
        },
    ],
    function(err, results) {
        if (err) {
            logger.error(request.userName + ":commonWorkflow:createPaymentCsvFile:Exit");
            responseCallback(err);
        } else {
            logger.info(request.userName + ":commonWorkflow:createPaymentCsvFile:Exit");
            responseCallback(results);
        }
    });
};


module.exports = commonRequest;