/*
 *******************************************************************************
 *
 *          %name:  common_model.js %
 *    %derived_by:  Nikhil %
 *
 *       %version:  1 %
 *       %release:  youtubeSync/1.0 %
 * %date_modified:  Mon Jan 13 14:00:00 2020 %
 *
 * Date          By             Description
 * ------------  -----------    -----------
 * Jan 13, 2020  Nikhil           created
 *******************************************************************************
 */
/*jslint node: true */
"use strict";
var commonModel = {};
var config = require("../../config/config");
var mysql_db = require("../../db/mysql_db");
var logger = config.logger;

commonModel.insertSessionInfo = function(userName, url, keywords, modelCallback) {
  var failureResponse = {
    responseCode: -1,
    response: {},
    errorMsg: "DB_ERROR"
  };
  var successResponse = {
    responseCode: 0,
    response: {},
    successMsg: "SUCCESS"
  };
  mysql_db.doConnect(function(err, connection) {
    if (err) {
      logger.error(userName + ":commonModel:insertSessionInfo:Error:" + err);
      failureResponse.errorMsg = "DB_CONNECTION_ERROR";
      mysql_db.doRelease(connection, function() {
        modelCallback(failureResponse);
      });
    } else {
      keywords = keywords ? keywords.join(",") : "";
      var sql = "insert into SESSION (USER_SESSION_ID,YOUTUBE_URL,KEYWORDS) values (?,?,?)";
      mysql_db.executeSql(
        connection,
        {
          sql: sql,
          bindParams: [userName, url, keywords]
        },
        function(err_1) {
          if (err_1) {
            logger.error(userName + ":commonModel:insertSessionInfo:Error:" + err_1);
            mysql_db.doRelease(connection, function() {
              modelCallback(failureResponse);
            });
          } else {
            logger.info(userName + ":commonModel:insertSessionInfo:success", successResponse);
            mysql_db.doRelease(connection, function() {
              modelCallback(successResponse);
            });
          }
        }
      );
    }
  });
};
/*
Insert Chat message on stream using callback function
*/
commonModel.insertChat = function(userName, request, modelCallback) {
  var failureResponse = {
    responseCode: -1,
    response: {},
    errorMsg: "DB_ERROR"
  };
  var sucessResponse = {
    responseCode: 0,
    response: {},
    successMsg: "SUCCESS"
  };
  mysql_db.doConnect(function(err, connection) {
    if (err) {
      logger.error(userName + ":commonModel:insertChat:Error:" + err);
      failureResponse.errorMsg = "DB_CONNECTION_ERROR";
      mysql_db.doRelease(connection, function() {
        modelCallback(failureResponse);
      });
    } else {
      var sql = "insert into CHAT (USER_SESSION_ID,MESSAGE) values (?,?)";
      mysql_db.executeSql(
        connection,
        {
          sql: sql,
          bindParams: [request.userId, request.message]
        },
        function(err_1) {
          if (err_1) {
            logger.error(userName + ":commonModel:insertChat:Error:" + err_1);
            mysql_db.doRelease(connection, function() {
              modelCallback(failureResponse);
            });
          } else {
            logger.info(userName + ":commonModel:insertChat:success", sucessResponse);
            mysql_db.doRelease(connection, function() {
              modelCallback(sucessResponse);
            });
          }
        }
      );
    }
  });
};

/*
Fetch Chat History using callback function
*/
commonModel.chatHistory = function(userId, modelCallback) {
  var failureResponse = {
    responseCode: -1,
    response: {},
    errorMsg: "DB_ERROR"
  };
  var sucessResponse = {
    responseCode: 0,
    response: [],
    successMsg: "SUCCESS"
  };
  mysql_db.doConnect(function(err, connection) {
    if (err) {
      logger.error(userId + ":commonModel:chatHistory:Error:" + err);
      failureResponse.errorMsg = "DB_CONNECTION_ERROR";
      mysql_db.doRelease(connection, function() {
        modelCallback(failureResponse);
      });
    } else {
      var sql = "SELECT MESSAGE FROM CHAT WHERE USER_SESSION_ID=?";
      mysql_db.executeSql(
        connection,
        {
          sql: sql,
          bindParams: [userId]
        },
        function(err_1, results) {
          if (err_1) {
            logger.error(userId + ":commonModel:chatHistory:Error:" + err_1);
            mysql_db.doRelease(connection, function() {
              modelCallback(failureResponse);
            });
          } else {
            if (results && results.length > 0) {
              results.forEach(result => {
                sucessResponse.response.push(result.MESSAGE);
              });
            }
            logger.info(userId + ":commonModel:chatHistory:success", sucessResponse);
            mysql_db.doRelease(connection, function() {
              modelCallback(sucessResponse);
            });
          }
        }
      );
    }
  });
};

module.exports = commonModel;
