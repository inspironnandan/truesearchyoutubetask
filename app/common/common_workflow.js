/*
 *******************************************************************************
 *
 *          %name:  common_workflow.js %
 *    %derived_by:  Nikhil %
 *
 *       %version:  1 %
 *       %release:  youtubeSync/1.0 %
 * %date_modified:  Mon Jan 13 14:00:00 2020 %
 *
 * Date          By             Description
 * ------------  -----------    -----------
 * Jan 13, 2020  Nikhil           created
 *******************************************************************************
 */
/*jslint node: true */
"use strict";
var commonWorkflow = {};
var commonModel = require("./common_model");
var config = require("../../config/config");
var logger = config.logger;
var request = require("request");

var { google } = require("googleapis");
const youtube = google.youtube("v3");
const OAuth2 = google.auth.OAuth2;
const clientId = "77669081051-9ah2a46d46t5vattnq3771arm5ebthde.apps.googleusercontent.com";
const clientSecret = "Y6pNxIWq82BOVDQElBvOYaEO";
const redirectURI = "http://ec2-13-235-9-102.ap-south-1.compute.amazonaws.com:9009/callback";
// const scope = [
//   "https://www.googleapis.com/auth/youtube.readonly",
//   "https://www.googleapis.com/auth/youtube",
//   "https://www.googleapis.com/auth/youtube.force-ssl",
//   "https://www.googleapis.com/auth/userinfo.email"
// ];

//let liveChatId;
let nextPage;
const intervalTime = 10000;
let interval;

/*
Fetch User info based on access token
*/
const _fetchUserInfo = accessToken => {
  return new Promise((resolve, reject) => {
    try {
      let options = {
        url: "https://www.googleapis.com/oauth2/v1/userinfo",
        method: "GET",
        qs: {
          access_token: accessToken
        }
      };
      request(options, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          logger.info("User Info:", body);
          var info = JSON.parse(body);
          return resolve(info.email);
        } else {
          return reject(error);
        }
      });
    } catch (err) {
      return reject(err);
    }
  });
};

/*
Set Credentials for User passed from frontend
*/
commonWorkflow.authorize = async req => {
  logger.info("Authorize Entered");
  const auth = new OAuth2(clientId, clientSecret, redirectURI);
  const response = {
    responseCode: 0,
    response: {
      auth: {}
    },
    msg: ""
  };
  return new Promise((resolve, reject) => {
    try {
      const tokens = {
        access_token: req.access_token,
        scope: req.scope,
        token_type: req.token_type,
        id_token: req.id_token
      };
      auth.setCredentials(tokens);
      logger.info("Set Credentials Successful");
      response.response.user = req;
      resolve(response);
    } catch (err) {
      response.responseCode = -1;
      response.msg = "Authorization Not Set";
      logger.error("Error Authorizing the user", err);
      reject(err);
    }
  });
};

/*
Generate liveChatId from provided link and fetch messages at regular interval
*/
commonWorkflow.findChatBasedOnId = async req => {
  const response = {
    responseCode: 0,
    response: {},
    msg: ""
  };
  try {
    const auth = new OAuth2(clientId, clientSecret, redirectURI);
    const tokens = {
      access_token: req.user.access_token,
      scope: req.user.scope,
      token_type: req.user.token_type,
      id_token: req.user.id_token
    };
    auth.setCredentials(tokens);
    let liveChatId;
    if (req.link) {
      let link = req.link.split("?");
      link = link[1] ? link[1].split("&") : null;
      let id = link[0];
      let ids = id.split("=");
      const youTubeResponse = await youtube.liveBroadcasts.list({
        auth,
        part: "snippet,contentDetails,status",
        id: ids[1]
      });
      const userId = await _fetchUserInfo(req.user.access_token);
      const latestChat = youTubeResponse.data.items[0];
      liveChatId = latestChat.snippet.liveChatId;
      if (liveChatId) {
        if (interval) {
          logger.info("Clearing Old Interval");
          clearInterval(interval);
        }
        logger.info("Chat ID Found:", liveChatId);
        commonModel.insertSessionInfo(userId, req.link, req.keywords, function(resp) {
          logger.info("Insert Session Info: ", resp);

          interval = setInterval(function() {
            getChatMessagesOnId(req, liveChatId, userId);
          }, intervalTime);
        });
      } else {
        response.responseCode = -1;
        response.msg = "No Chat ID Found";
        logger.error("No Chat ID found");
        return Promise.reject(response);
      }
    } else {
      response.responseCode = -1;
      response.msg = "No Auth Found";
      logger.info("No Auth Found");
      return Promise.reject(response);
    }
  } catch (err) {
    return Promise.reject(err);
  }
};

/*
Socket connection and Message Transmit to front end. 
*/
const getChatMessagesOnId = async (req, liveChatId, userId) => {
  const failureResponse = {
    responseCode: -1,
    response: {},
    errMsg: ""
  };
  const auth = new OAuth2(clientId, clientSecret, redirectURI);
  const tokens = {
    access_token: req.user.access_token,
    scope: req.user.scope,
    token_type: req.user.token_type,
    id_token: req.user.id_token
  };
  auth.setCredentials(tokens);
  var { socket } = require("../../youtubeSync");
  if (socket) {
    const response = await youtube.liveChatMessages.list({
      auth,
      part: "snippet",
      liveChatId,
      pageToken: nextPage
    });

    const { data } = response;
    const newMessages = data.items;

    newMessages.forEach(message => {
      const request = {
        userId,
        message: message.snippet.displayMessage
      };
      logger.info("Keywords Extracted", req.keywords);
      if (Array.isArray(req.keywords) && req.keywords.length > 0) {
        const doesExist = req.keywords.some(x => {
          return message.snippet.displayMessage.includes(x);
        });
        if (doesExist) {
          logger.info("Message to be Emitted: ", message.snippet.displayMessage);
          socket.emit("chat", request.message);
        }
      } else {
        logger.info("No Keywords Found: Emitting All the messages: ");
        logger.info("Message to be Emitted: ", message.snippet.displayMessage);
        socket.emit("chat", request.message);
      }
      commonModel.insertChat(userId, request, function(resp) {
        logger.info("resp>>", resp);
      });
    });
    nextPage = data.nextPageToken;
    logger.info("Fetching Chat Messages.....");
  } else {
    logger.error("No Socket Found");
    failureResponse.errMsg = "No Socket Found";
    return Promise.reject(failureResponse);
  }
};

/*
Fetch Chat History from DB
*/

const _fetchChatHistory = async userId => {
  return new Promise((resolve, reject) => {
    try {
      commonModel.chatHistory(userId, function(resp) {
        return resolve(resp);
      });
    } catch (err) {
      return reject(err);
    }
  });
};

/*
Fetch User Id to get chat history
*/
commonWorkflow.chatHistory = async req => {
  const successResponse = {
    responseCode: 0,
    response: [],
    message: ""
  };
  try {
    const userId = await _fetchUserInfo(req.user.access_token);
    logger.info("User Id", userId);
    const fetchChatHistory = await _fetchChatHistory(userId);
    if (fetchChatHistory.response.length > 0 && Array.isArray(req.keywords) && req.keywords.length > 0) {
      fetchChatHistory.response.forEach(chat => {
        const doesExist = req.keywords.some(x => {
          return chat.includes(x);
        });
        if (doesExist) {
          successResponse.response.push(chat);
        }
      });
    } else {
      successResponse.response = fetchChatHistory.response;
    }
    return Promise.resolve(successResponse);
  } catch (err) {
    return Promise.reject(err);
  }
};

module.exports = commonWorkflow;
