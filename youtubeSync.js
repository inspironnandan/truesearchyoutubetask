/*
 *******************************************************************************
 *
 *          %name:  youtubeSync.js %
 *    %derived_by:  Nikhil %
 *
 *       %version:  1 %
 *       %release:  youtubeSync/1.0 %
 * %date_modified:  Mon Jan 13 14:00:00 2020 %
 *
 * Date          By             Description
 * ------------  -----------    -----------
 * Jan 13, 2020  Nikhil 	      created
 *******************************************************************************
 */
/*jslint node: true */
"use strict";
var express = require("express");
var app = express(); // create our app w/ express
var database = require("./db/mysql_db");
var config = require("./config/config"); // load the  config
var bodyParser = require("body-parser");
var port = process.env.PORT || config.SERVER.PORT; // set the port
var environment = process.env.NODE_ENV || config.SERVER.ENVIRONMENT;
var server;
var logger = config.logger;
var path = require("path");
var commonRoute = require("./routes/common/commonRoute");
const socket = require("socket.io");
let io;

// configuration ===============================================================
// Remove winston's console logger in production mode.
/* istanbul ignore next */
if (logger.loggers.default.transports.length > 0) {
  if (logger.loggers.default.transports[0].name === "console" && environment === "production" && !config.LOG.CONSOLE_PRINT) {
    try {
      logger.remove(logger.transports.Console);
    } catch (err) {}
  }
}

global.appRoot = path.resolve(__dirname);
// Common middleware.
app.use(require("./middleware/common_middleware"));
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }));

/**
 * Security Settings.
 */

app.disable("x-powered-by");
app.enable("trust proxy"); //Enable only when behind nginx.

app.set("title", config.APP_TITLE);
app.set("version", config.APP_VERSION);
app.set("port", port);
app.set("env", environment);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,username,apiname,Authorization");
  res.header("Access-Control-Expose-Headers", "Content-Disposition");
  next();
});

app.use("/", commonRoute);

function startServer() {
  logger.info("SERVER - Starting process...", {
    title: app.get("title"),
    version: app.get("version"),
    port: app.get("port"),
    NODE_ENV: app.get("env"),
    pid: process.pid
  });
  server = app.listen(app.get("port"));
  io = socket(server);
  server.timeout = config.SERVER.TIMEOUT;
  logger.info("App listening on port " + port);
}

try {
  database.init(function(e) {
    console.log("initializing");
    if (e) {
      throw e;
    } else {
      startServer();
    }
  });
} catch (err) {
  logger.error("Error occured: " + err.stack);
  //Shutdown now
  shutdown();
}

process.on("SIGINT", function() {
  logger.error("SIGINT Recived");
  shutdown();
});

/* istanbul ignore next */
process.on("SIGTERM", function() {
  logger.error("SIGTERM Recived");
  shutdown();
});

process.on("uncaughtException", function(err) {
  logger.error("Error occured: " + err.stack);
  shutdown();
});

function shutdown() {
  logger.info("Shutting down ");
  process.exit(0);
}

/*Making Socket Connection*/
io.on("connection", function(socket) {
  logger.info("Socket Connect Established", socket.id);
  module.exports = { socket: socket };
});
