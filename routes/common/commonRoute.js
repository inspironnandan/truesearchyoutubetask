/*
 *******************************************************************************
 *
 *          %name:  commonRoute.js %
 *    %derived_by:  Nikhil %
 *
 *       %version:  1 %
 *       %release:  youtubeSync/1.0 %
 * %date_modified:  Mon Jan 13 14:00:00 2020 %
 *
 * Date          By             Description
 * ------------  -----------    -----------
 * Jan 13, 2020  Nikhil           created
 *******************************************************************************
 */
/*jslint node: true */
"use strict";
var express = require("express");
var commonRoute = express.Router(); // create our app w/ express
var config = require("../../config/config"); // load the  config
var logger = config.logger;
var api = require("../../lib/api");
//var jwt = require('jsonwebtoken');
var commonWorkflow = require("../../app/common/common_workflow");

commonRoute.get("/authorize", async function(req, res) {
  try {
    const results = await commonWorkflow.getCode(res);
    console.log("results", results);
    res.redirect(results);
  } catch (err) {
    return api.ok(req, res, err);
  }
});

commonRoute.get("/callback", (req, res) => {
  res.redirect("/");
});

commonRoute.post("/loginDetails", async function(req, res) {
  try {
    logger.info(":commonRoute:get:authorize:Request Received", JSON.stringify(req.body));
    const results = await commonWorkflow.authorize(req.body);
    return api.ok(req, res, results);
  } catch (err) {
    return api.ok(req, res, err);
  }
});

commonRoute.get("/start-tracking-chat", (req, res) => {
  commonWorkflow.startTrackingChat();
  res.redirect("/");
});
commonRoute.post("/trackingChat", async (req, res) => {
  try {
    logger.info("Tracking Chat Received:", req.body);
    const response = {
      responseCode: 0,
      response: {},
      msg: ""
    };
    const results = commonWorkflow.findChatBasedOnId(req.body);
    console.log("results", results);
    return api.ok(req, res, response);
  } catch (err) {
    return api.ok(req, res, err);
  }
});

commonRoute.post("/chatHistory", async (req, res) => {
  try {
    const results = await commonWorkflow.chatHistory(req.body);
    console.log("results", results);
    return api.ok(req, res, results);
  } catch (err) {
    return api.ok(req, res, err);
  }
});

module.exports = commonRoute;
